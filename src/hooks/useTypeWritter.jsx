import React, {useState, useEffect} from 'react';

const sleep = async ms => {
    return new Promise((resolve, reject) => setTimeout(resolve, ms) )
}

const useTypewritter = (str) => {
    const [_string, set_string] = useState('');
    useEffect(() => {
        const type = async () => {
            for (let char in str + '_') {
                await sleep(500);
                let s = str.slice(0, char);
                set_string(s);
            }
        }
        type();
    }, [])
    return [_string];
}

export default useTypewritter;